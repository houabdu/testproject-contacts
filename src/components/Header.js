import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Dimensions } from "react-native";
import { Colors, IconButton } from "react-native-paper";
import { useNavigation } from "@react-navigation/native";

const {width} = Dimensions.get('window')

const Header = ({ isFavorite, onFavorite, isDisabled, onDisable }) => {
  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.backButtonContainer}
        onPress={() => navigation.openDrawer()}
      >
        <>
          <IconButton icon="chevron-left" size={24} style={{ margin: 0 }} />
          <Text style={styles.backButton}>All Contacts</Text>
        </>
      </TouchableOpacity>
      <View style={styles.controlButtons}>
        <IconButton
          icon="star"
          color={isFavorite ? Colors.yellow900 : "black"}
          size={30}
          onPress={onFavorite}
        />
        <IconButton
          icon="account-edit"
          color={isDisabled ? "black" : Colors.yellow900}
          size={30}
          onPress={onDisable}
        />
      </View>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  container: {
    width,
    flexDirection: "row",
    justifyContent: 'space-between',
    paddingHorizontal: 12,
    alignItems: 'flex-start'
  },
  backButtonContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  backButton: {
    color: "black",
    fontSize: 18,
  },
  controlButtons: {
    flexDirection: 'row'
  }
});
