import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { StatusBar } from "expo-status-bar";
import Header from "../components/Header";
import { Avatar, TextInput } from "react-native-paper";
import { useRoute } from "@react-navigation/native";
import moment from 'moment';
import useContact from "../hook/useContact";

const DetailPage = () => {
  const { params } = useRoute();
  const [contact, setContact] = useContact(params);
  const [locked, setLocked] = useState(true);

  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <Header
        isDisabled={locked}
        onDisable={() => setLocked((prev) => !prev)}
        isFavorite={contact?.favorite}
        onFavorite={() => setContact(!contact?.favorite, "favorite")}
      />
      {contact && (
        <View style={styles.bodyContainer}>
          <View style={styles.firstContainerWithAvatar}>
            <Avatar.Image
              style={styles.avatar}
              size={150}
              source={{ uri: contact?.largeImageURL }}
            />
            <View style={styles.inputContainerFirst}>
              <TextInput
                label="Name"
                disabled={locked}
                style={styles.textInput}
                value={contact.name}
                onChangeText={(text) => setContact(text, "name")}
              />
              <TextInput
                label="Company"
                disabled={locked}
                style={styles.textInput}
                value={contact.company}
                onChangeText={(text) => setContact(text, "company")}
              />
            </View>
          </View>
          <View style={styles.phoneNumberInputContainer}>
            <TextInput
              label="Work"
              disabled={locked}
              style={styles.textInput}
              value={contact.phone.work}
              onChangeText={(text) => setContact(text, "phone", "company")}
            />
            <TextInput
              label="Home"
              disabled={locked}
              style={styles.textInput}
              value={contact.phone.home}
              onChangeText={(text) => setContact(text, "phone", "company")}
            />
            <TextInput
              label="Mobile"
              disabled={locked}
              style={styles.textInput}
              value={contact?.phone.mobile}
              onChangeText={(text) => setContact(text, "phone", "company")}
            />
          </View>
          <View style={styles.phoneNumberInputContainer}>
            <TextInput
              label="Work"
              disabled={locked}
              style={styles.textInput}
              value={contact.address.state}
              onChangeText={(text) => setContact(text, "address", "state")}
            />
            <TextInput
              label="Home"
              disabled={locked}
              style={styles.textInput}
              value={contact?.address.zip}
              onChangeText={(text) => setContact(text, "address", "zip")}
            />
            <TextInput
              label="Mobile"
              disabled={locked}
              style={styles.textInput}
              value={contact.address.city}
              onChangeText={(text) => setContact(text, "address", "city")}
            />
          </View>
          <TextInput
            label="Street"
            disabled={locked}
            style={styles.textInput}
            value={contact.address.street}
            onChangeText={(text) => setContact(text, "address", "street")}
          />
          <TextInput
            label="Birthday"
            disabled
            style={styles.textInput}
            value={moment(contact.birthdate * 1).format('DD.MM.YYYY')}
            onChangeText={(text) => setContact(text, "birthdate")}
          />
          <TextInput
            label="Email"
            disabled={locked}
            style={styles.textInput}
            value={contact.email}
            onChangeText={(text) => setContact(text, "email")}
          />
        </View>
      )}
    </View>
  );
};

export default DetailPage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    paddingTop: 50,
    alignItems: "flex-start",
    justifyContent: "flex-start",
  },
  bodyContainer: {
    justifyContent: "flex-start",
    alignItems: "flex-start",
    paddingTop: 24,
    paddingLeft: 20,
    paddingRight: 12,
  },
  firstContainerWithAvatar: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginBottom: 12,
  },
  inputContainerFirst: {
    flex: 1,
    paddingLeft: 24,
    marginBottom: 12,
  },
  textInput: {
    flexGrow: 1,
    alignSelf: "stretch",
    maxHeight: 60,
    marginBottom: 12,
    marginRight: 8,
  },
  phoneNumberInputContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginBottom: 8,
  },
});
