import React from "react";
import { View, FlatList, StyleSheet } from "react-native";
import { List, Avatar } from "react-native-paper";

const ContactList = ({ data, navigation }) => {
  return (
    <FlatList
      style={styles.container}
      showsVerticalScrollIndicator={false}
      data={data}
      renderItem={({ item }) => (
        <List.Item
          key={`noId.${item.name}.${item.address.city}`}
          title={item.name}
          description={item.phone.home}
          onPress={() => {
            navigation.navigate('Home', item);
          }}
          left={() => (
            <Avatar.Image size={50} source={{ uri: item.smallImageURL }} />
          )}
        />
      )}
      ListFooterComponent={() => <View style={styles.emptyBox} />}
    />
  );
};

export default ContactList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 50,
    paddingLeft: 12,
  },
  emptyBox: {
    height: 100
  }
});
