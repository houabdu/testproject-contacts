import { useEffect, useState } from "react";

const useContact = (data) => {
  const [contact, setContactState] = useState();

  useEffect(() => {
      setContactState(data);
  }, [data])

  const setContact = (newValue, key1, key2) => {
    setContactState((prev) => {
      let prevContact = {
        ...prev,
        [key1]: key2
          ? {
              [key2]: newValue,
            }
          : newValue,
      };
      return prevContact;
    });
  };

  return [contact, setContact];
};

export default useContact;
