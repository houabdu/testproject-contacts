import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { NavigationContainer } from "@react-navigation/native";
import useFetch from "react-fetch-hook";

import DetailPage from "./src/screen/DetailPage";
import ContactList from "./src/screen/ContactList";

const Drawer = createDrawerNavigator();

export default function App() {
  const { data } = useFetch("https://waltken.de/Contacts_v2.json");

  if (!data) {
    return null;
  }

  return (
    <NavigationContainer>
      <Drawer.Navigator
        initialRouteName="Home"
        openByDefault
        drawerContent={(props) => <ContactList data={data} {...props} />}
      >
        <Drawer.Screen name="Home" component={DetailPage} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}
